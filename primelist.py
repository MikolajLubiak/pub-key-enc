#!/usr/bin/env python

from isprime import isPrime

def primeList(n: int):
    primes = []
    for i in range(n):
        if isPrime(i):
            primes.append(i)
    return tuple(primes)
