#!/usr/bin/env python

from genprime import genPrime
from sys import argv
from functools import reduce
from operator import mul

if argv[1] == "gen":
    privkey = (genPrime(), genPrime())
    print(f"---PRIVKEY1---\n\n{privkey[0]}\n\n---PRIVKEY1---\n\n\n\n---PRIVKEY2---\n\n{privkey[1]}\n\n---PRIVKEY2---")
    print("\n"*6)
    pubkey = privkey[0] * privkey[1]
    print(f"---PUBKEY---\n\n{pubkey}\n\n---PUBKEY---")
elif argv[1] == "enc":
    msg = bytes(argv[2], 'utf-8')
    pubkey = int(argv[3])
    enc = reduce(mul, msg, pubkey)    
    print(enc)
elif argv[1] == "dec":
    msg = int(argv[2])
    privkey = tuple(map(int, argv[3].split(" ")))
    dec = msg / reduce(mul, privkey)
    print(dec)
